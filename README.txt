TRIUMF Docushare Modules
---------------------------
This is a suite of Drupal modules for the TRIUMF website that provide access to some Docushare features.
All modules are part of the 'docushare-api' package.

ABOUT
---------------------------
I'm Colin, a fourth year Computer Science student. I worked on this project during my Fall 2015 work term.
If you have any questions about this suite, feel free to shoot me an email at dignazic@myumanitoba.ca


OVERVIEW
---------------------------
The custom modules include are as follows:

doculogin - used for logging into Docushare

docusearch - allows searching Docushare by keyword
keyword_search - form users can use to search Docushare by one or two keywords
project_search - form users can use to search Docushare by project
excelexport - allows users to export search results to a spreadsheet

documodify - used with project_search for modifying document properties
propfind - senc PROPFIND request to docushare, used with documodify

docuview - template to display predefined TRIUMF document sets
view_manuals - display TRIUMF manuals
view_tsn - display TRIUMF Safety Notes
view_tsop - display TRIUMF Standard Operating Proceedures

phpexcel is a third party Drupal module required by excelexport that is not included in this suite.
Download phpexcel here: https://www.drupal.org/node/1351450
Also ensure that the PHP library PHPExcel is installed on the server as well: http://phpexcel.codeplex.com/

INSTALLATION
---------------------------
1) Copy all modules to sites/all/modules
2) Download the third party Drupal module phpexcel: https://www.drupal.org/node/1351450
3) Install the PHP library PHPExcel on the server: http://phpexcel.codeplex.com/
4) Enable all modules


USAGE
---------------------------
**Note: To execute PHP functions on Drupal pages the page's "Text format" must be set to "PHP Code".**

Forms in this suite can be called individually using:
    <?php print drupal_render(drupal_get_form(<form name>));

For example the following code would display the Docushare login form:
    <?php print drupal_render(drupal_get_form('doculogin_form'));

Inclued in this suite are a few functions to print useful pages. For example, the following will print the project search
form along with a login form and relevant copy:
    <?php project_search_print_page();

Here is a list of all predefined "page" functions:

documodify_print_page()
keyword_search_print_page()
project_search_print_page()
view_manuals_print_page()
view_tsn_print_page()
view_tsop_print_page()

Here is the code for a sample "index" page for this suite:

    Search Docushare
    <ul>
        <li><a href="/project-management/keyword-search">Keyword Search</a></li>
        <li><a href="/project-management/project-search">Project Search</a></li>
    </ul>
    <br/>
    View Docushare Document Sets
    <ul>
        <li><a href="/project-management/triumf-safety-notes">TRIUMF Safety Notes (TSN)</a></li>
        <li><a href="/project-management/triumf-standard-operating-procedures">TRIUMF Standard Operating Procedures (TSOP)</a></li>
        <li><a href="/project-management/triumf-manuals">TRIUMF Manuals</a></li>
    </ul>
    <br/>
    Administration
    <ul>
        <li><a href="/project-management/modify-document-properties">Modify Document Properties</a></li>
    </ul>
    <?php print drupal_render(drupal_get_form('doculogin_form'));
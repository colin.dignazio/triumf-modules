<?php

/*
 * docusearch.module
 *
 * Send a search request to Docushare and parse the response.
 *
 * Used mainly for the docusearch_handle_search function by the search form modules.
 */

//Sends the search request to Docusearch and returns the response data.
function docusearch_create_search_request($xml) {
  $url = "https://documents.triumf.ca/docushare/dsweb/SEARCH";

  $options = array(
    'method' => 'POST',
    'data' => $xml,
    'headers' => array(
      'Content-Type' => 'text/xml',
      "Accept-Language" => "en",
      "DocuShare-Version" => "5.0",
      "Accept" => "text/xml",
      "Cookie" => $_COOKIE[DOCULOGIN_COOKIE_TOKEN]
    ),
    'timeout' => 120
  );

  $response = drupal_http_request($url, $options);

  return $response->data;
}

//Default parsing function to use if one is not defined.
function docusearch_parse_data($xml) {
  $html = "<ul>";

  foreach ($xml->response as $elem) {
    if (isset($elem->propstat->prop->acl)) {
      $html .= "<li><a href='https://documents.triumf.ca/docushare/dsweb/Get/";
      $html .= (string)$elem->propstat->prop->acl->attributes() . "'>";
      $html .= $elem->propstat->prop->displayname . "</a><br/>";
      $html .= "<p style='font-size:80%;'>" . $elem->propstat->prop->summary . "</p></li>";
    }
  }

  $html .= "</ul>";
  return $html;
}

/*
 * docusearch_handle_search
 *
 * Does all the "dirty work" of making a search request.
 *
 * $keywords - The keywords to be searched for.
 * $operator - Either 'and' or 'or', defined either a intersection or union search.
 * $additional_properties - Extra properties required by the parse function.
 * $parse_function - The name of the function that will be used to parse the data.
 *
 * Returns HTML representing the search.
 */
function docusearch_handle_search($keywords, $operator, $orderby, $additional_properties = null, $parse_function = 'docusearch_parse_data') {
  $html = "";

  if($parse_function == null) {}

  if (isset($keywords)) {
    $searchxml = docusearch_build_keyword_query($keywords, null, $orderby, $operator, $additional_properties);
    $data = docusearch_create_search_request($searchxml);
    $html = call_user_func($parse_function, simplexml_load_string($data));
  }

  return $html;
}

//Build the XML for the search query.
function docusearch_build_keyword_query($keywords, $from = null, $orderby = "displayname", $operator = 'or', $additionalselect) {
  $xml = "";
  $xml .= "<?xml version=\"1.0\" ?>";
  $xml .= "<searchrequest><simplesearch><select><prop><displayname/><summary/><acl/>";

  foreach($additionalselect as $select) {
    $xml .= "<$select/>";
  }

  $xml .= "</prop></select>";

  if (isset($from)) {
    $xml .= "<from>";

    foreach ($from as $collection) {
      $xml .= "<scope><href>https://documents.triumf.ca/docushare/$collection</href></scope>";
    }

    $xml .= "</from>";
  }

  $xml .= "<limit><nresults>1000</nresults></limit>";
  $xml .= "<where><$operator>";

  if (isset($keywords['projno'])) {
    $xml .= "<or><contains><prop><keywords/></prop><literal>" . $keywords['projno'] . "</literal></contains>";
    $xml .= "<contains><prop><keywords/></prop><literal>" . $keywords['projno-truncated'] . "</literal></contains></or>";

    unset($keywords['projno']);
    unset($keywords['projno-truncated']);
  }

  foreach ($keywords as $key) {
    $xml .= "<contains><prop><keywords/></prop><literal>$key</literal></contains>";
  }

  $xml .= "</$operator></where>";

  if (isset($orderby)) {
    $xml .= "<orderby><sortspec><propname>" . $orderby . "</propname></sortspec><ascending/></orderby>";
  }

  $xml .= "</simplesearch></searchrequest>";

  return $xml;
}